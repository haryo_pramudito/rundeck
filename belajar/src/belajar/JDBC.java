package belajar;

import java.sql.*;

public class JDBC{
public Connection connect() {

    System.out.println("\n"+"-------- Oracle JDBC Connection Testing ------");

    try {

            Class.forName("oracle.jdbc.driver.OracleDriver");

    } catch (ClassNotFoundException e) {

            System.out.println("Where is your Oracle JDBC Driver?");
            e.printStackTrace();


    }

    System.out.println("\033[32m Oracle JDBC Driver Registered!\033"+"\n");

    Connection connection = null;

    try {

            connection = DriverManager.getConnection(
                            "jdbc:oracle:thin:@10.2.117.152:1521/OTDASH", "DASHBOARD","Tsel321");
    } catch (SQLException e) {

            System.out.println("\033[31mConnection Failed! Please Check Below Console!!!\033[0m \033[32;1");
            System.out.println("=============================================="+"\n");
            e.printStackTrace();


    }

    if (connection != null) {
            System.out.println("\033[32mCONNECT BRO!!!, you take control your database now!\033[0m \033[32;1");
    } else {
            System.out.println("Failed to make connection!");
    }

    return connection;
}


}

