package belajar;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import com.google.gson.JsonArray;


//import org.omg.CORBA.portable.InputStream;

import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class JSON {

	public static void main(String[] args) {
		
		 String job ="";
		 String log = "";
		 String id = "";
		 String desc = "";
		 String sdate= "";
		 String edate= "";
		 String Duration = "";
		 String status = "";
		 String desc2 = "";
		
		// TODO Auto-generated method stub
		String sURL = "http://datarecontapp1:4440/api/20/project/Telkomsel/executions?format=json&authtoken=JoX8hvLYT0KgK8n3AH1eHBRl8ddDUbSK"; //just a string
		
	    // Connect to the URL using java's native library
	    URL url = null;
		try {
			url = new URL(sURL);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    HttpURLConnection request = null;
		try {
			request = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try {
			request.connect();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    // Convert to a JSON object to print data
	    JsonParser jp = new JsonParser(); //from gson
	    JsonElement root = null;
		try {
			root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} //Convert the input stream to a json element
	    JsonObject rootobj = root.getAsJsonObject(); //May be an array, may be an object. 
	    JsonArray execution = rootobj.get("executions").getAsJsonArray(); 
	
	   
	    for (int i=0; i < execution.size(); i++) {
	    		
	    		JsonObject inside = (JsonObject) execution.get(i);
	    		
	    		try {
	    		JsonObject jobobj = inside.get("job").getAsJsonObject();
	    		job = jobobj.get("name").getAsString();
	    		//System.out.println(job);
	    		}catch (NullPointerException nl) {
	    			job=null;
	    		}
	    		

	    	    JsonObject idobj = inside.getAsJsonObject();
	    	    id = idobj.get("id").getAsString();
	    	    //System.out.println(id);
	    	    
	    	    JsonObject sdateobj = inside.get("date-started").getAsJsonObject();
	    	    sdate = sdateobj.get("date").getAsString();
	    	    sdate = sdate.replace("T"," ");
	    	    sdate = sdate.replace("Z"," ");
	    	    //System.out.println(sdate);
	    	    
	    	    JsonObject edateobj = inside.get("date-ended").getAsJsonObject();
	    	    edate = edateobj.get("date").getAsString();
	    	    edate = edate.replace("T"," ");
	    	    edate = edate.replace("Z"," ");
	    	    //System.out.println(edate);
	    	    
	    	    try {
	    	    JsonObject descobj2 = inside.get("job").getAsJsonObject();
	    		desc2 = descobj2.get("description").getAsString();
	    		//System.out.println(desc2);
	    	    }catch (NullPointerException nl) {
	    	    		job=null;
	    	    }
	    		
	    	    
	    	    JsonObject sobj = inside.getAsJsonObject();
	    	    status = sobj.get("status").getAsString();
	    	    //System.out.println(status);
	    	    
	    	    JsonObject descobj = inside.getAsJsonObject();
	    	    desc = descobj.get("description").getAsString();
	    	    //System.out.println(desc);
	    	    
	    	    
	    	    //date
	    	    	String iURL = "http://datarecontapp1:4440/api/20/execution/"+id+"/output?format=json&authtoken=JoX8hvLYT0KgK8n3AH1eHBRl8ddDUbSK"; //just a string
	    		
	    	    URL iurl = null;
	    		try {
	    			iurl = new URL(iURL);
	    		} catch (MalformedURLException e) {
	    			
	    			e.printStackTrace();
	    		}
	    	    HttpURLConnection irequest = null;
	    		try {
	    			irequest = (HttpURLConnection) iurl.openConnection();
	    		} catch (IOException e) {
	    			
	    			e.printStackTrace();
	    		}
	    	    try {
	    			irequest.connect();
	    		} catch (IOException e) {
	    			
	    			e.printStackTrace();
	    		}

	    	   
	    	    JsonParser ijp = new JsonParser(); 
	    	    JsonElement iroot = null;
	    		try {
	    			iroot = ijp.parse(new InputStreamReader((InputStream) irequest.getContent()));
	    		} catch (JsonIOException e) {
	    		
	    			e.printStackTrace();
	    		} catch (JsonSyntaxException e) {
	    			
	    			e.printStackTrace();
	    		} catch (IOException e) {
	    			
	    			e.printStackTrace();
	    		} 
			JsonObject iobj = iroot.getAsJsonObject(); 
			
			JsonObject dobj = iroot.getAsJsonObject();
			Duration = dobj.get("execDuration").getAsString();
    	    		//System.out.println(Duration);
			
	    	    	JsonArray entries = iobj.get("entries").getAsJsonArray(); 
	    	    
	    	    		
	    	    for (int b=0; b < entries.size();b++) {
	    	    		
	    	    		JsonObject bobj = (JsonObject) entries.get(b);
	    	    		
	    	    		JsonObject logobj = bobj.getAsJsonObject();
	    	    		log += logobj.get("log").getAsString();
	    	    		
	    	    		
	    	    		
	    	    		//System.out.println(log);
	    	    		
	    	    }
	    	    
	    	    
	    	    
	    	    
	    	    
	    	    insert(id,job,desc,sdate,edate,log,Duration,desc2,status); 
	    }
	    
	    
	}
	
	public static void insert(String id, String job, String desc, String sdate, String edate, String log, String Duration, String desc2, String status) {
		
		JDBC ntap = new JDBC();
		//ntap.connect();
		Connection connect = ntap.connect();
		
		String sql = "Insert Into mantap(transaction_id, job_name, command, start_date, end_date, logs, durations, job_desc, job_status ) values (?,?,?,to_timestamp(?,'yyyy/mm/dd hh24:mi:ss'),to_timestamp(?,'yyyy/mm/dd hh24:mi:ss'),?,?,?,?)";
		try {
			PreparedStatement ps = connect.prepareStatement(sql);
			ps.setInt(1, Integer.parseInt(id));
			ps.setString(2, job);
			ps.setString(3, desc);
			ps.setString(4, sdate);
			ps.setString(5, edate);
			ps.setString(6, log);
			ps.setString(7, Duration);
			ps.setString(8, desc2);
			ps.setString(9, status);
			ps.executeUpdate();
			
		} catch (SQLException ex) {
		      System.out.println(ex.getMessage());
		}
		
		}
		
		
	}


