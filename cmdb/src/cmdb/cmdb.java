package cmdb;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;

public class cmdb {

	public static void main(String[] argv) {

		System.out.println("\n"+"-------- Oracle JDBC Connection Testing ------");

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");

		} catch (ClassNotFoundException e) {

			System.out.println("Where is your Oracle JDBC Driver?");
			e.printStackTrace();
			return;

		}

		System.out.println("\033[32m Oracle JDBC Driver Registered!\033"+"\n");

		Connection connection = null;

		try {

			connection = DriverManager.getConnection(
		    			"jdbc:oracle:thin:@exapdb62b-scan.telkomsel.co.id:1521:OPCMDB1", "CMDB","g7HrASNZ");
		} catch (SQLException e) {

			System.out.println("\033[31mConnection Failed! Please Check Below Console!!!\033[0m \033[32;1");
			System.out.println("=============================================="+"\n");
			e.printStackTrace();
			return;

		}

		if (connection != null) {
			System.out.println("\033[32mCONNECT BRO!!!, you take control your database now!\033[0m \033[32;1");
		} else {
			System.out.println("Failed to make connection!");
		}
	}

}
